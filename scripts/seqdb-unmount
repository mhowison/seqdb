#!/bin/bash
#
# SeqDB - storage model for Next Generation Sequencing data
#
# Copyright 2011-2014, Brown University, Providence, RI. All Rights Reserved.
#
# This file is part of SeqDB.
#
# Permission to use, copy, modify, and distribute this software and its
# documentation for any purpose other than its incorporation into a
# commercial product is hereby granted without fee, provided that the
# above copyright notice appear in all copies and that both that
# copyright notice and this permission notice appear in supporting
# documentation, and that the name of Brown University not be used in
# advertising or publicity pertaining to distribution of the software
# without specific, written prior permission.
#
# BROWN UNIVERSITY DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
# INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR ANY
# PARTICULAR PURPOSE.  IN NO EVENT SHALL BROWN UNIVERSITY BE LIABLE FOR
# ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

if [ $# -lt 1 ]; then
	echo "usage: seqdb-unmount MOUNT_PATH" >&2
	exit 0
fi

MOUNT=$1

FSTAB=$HOME/.seqdb.fstab
touch $FSTAB

set -o nounset
set -o errexit

if [ -e $MOUNT ]
then
	if [ -p $MOUNT ]
	then
		echo "seqdb-unmount: removing mount point $MOUNT" >&2
		rm $MOUNT
	else
		echo "seqdb-unmount: error: path is not a seqdb mount:
  $MOUNT" >&2
	fi
fi
awk "BEGIN { FS = \"\t\" } { if (\$3 != \"$MOUNT\") print \$0 }" $FSTAB >$FSTAB~
mv $FSTAB~ $FSTAB

